# Ansible Wireguard



### Generate priv and public key

```
privkey=$(wg genkey) sh -c 'echo "server_privkey: $privkey
server_pubkey: $(echo $privkey | wg pubkey)"' > group_vars/all.yml
```

### Change inventory for your values.

`ansible_host` and `ansible_ssh_private_key_file` for your values in _inventory.ini_


### Run ansible playbook

```
ansible-playbook -i inventory.ini server-playbook.yml
```
